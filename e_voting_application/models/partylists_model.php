<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Partylists_Model extends CI_Model {

    public $tbl;

    public function __construct() {
        parent::__construct();

        $this->config->load('db_tbl_config');
        $this->tbl = $this->config->item('db_tbl_partylists');

        if (!$this->db->table_exists($this->tbl)) {
            $this->_create_tbl_partylists();
        }
    }

    public function _get_partylists() {
        $this->db->select('*');
        $this->db->from($this->tbl);
        $query = $this->db->get();
        return $query->result();
    }

    public function _get_partylist($id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function _update_partylist($id, $obj) {
        $this->db->where('id', $id);
        $this->db->update($this->tbl, $obj);
        if ($this->db->affected_rows() > 0) {
            return $this->session->set_flashdata('success', $obj['name'] . ' successfully updated!');
        }
    }

    public function _create_partylist($obj) {
        $this->db->insert($this->tbl, $obj);
    }

    public function _create_tbl_partylists() {
        $this->load->dbforge();
        $this->db->query('SET storage_engine=MYISAM;');
        $this->dbforge->add_field('id INT(11) NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('name VARCHAR(200) NOT NULL');
        $this->dbforge->add_field('description VARCHAR(200) NOT NULL');
        $this->dbforge->add_field('deleted_flag INT(1) DEFAULT 0 NOT NULL');
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl);
    }

}

/* 
 * end of file 
 * location: models/partylists_model.php 
 */
    