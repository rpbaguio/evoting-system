<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Candidates extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Candidates',
            'candidates' => $this->candidates_model->_get_candidates()
        );
        $this->load->view('header', $obj);
        $this->load->view('candidates/candidates');
        $this->load->view('footer');
    }

    public function create() {
        $this->form_validation->set_rules('person_id', 'Person ID', 'trim|required|xss_clean|callback_candidate_exists');
        $this->form_validation->set_rules('position_id', 'Position', 'trim|required|xss_clean|callback_position_selection_empty');
        $this->form_validation->set_rules('partylist_id', 'Party List', 'trim|required|xss_clean|callback_partylist_selection_empty');
        if ($this->form_validation->run()) {
            $obj = array(
                'person_id' => $this->input->post('person_id'),
                'position_id' => $this->input->post('position_id'),
                'partylist_id' => $this->input->post('partylist_id')
            );
            $this->candidates_model->_create_candidate($obj);
            redirect('candidates', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Registration Form',
                'positions' => $this->positions_model->_get_positions(),
                'partylists' => $this->partylists_model->_get_partylists()
            );
            $this->load->view('header', $obj);
            $this->load->view('candidates/create');
            $this->load->view('footer');
        }
    }

    public function update() {
        $this->form_validation->set_rules('person_id', 'Person ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('position_id', 'Position', 'trim|required|xss_clean|callback_position_selection_empty');
        $this->form_validation->set_rules('partylist_id', 'Party List', 'trim|required|xss_clean|callback_partylist_selection_empty');
        if ($this->form_validation->run()) {
            $obj = array(
                'person_id' => $this->input->post('person_id'),
                'position_id' => $this->input->post('position_id'),
                'partylist_id' => $this->input->post('partylist_id')
            );
            $this->candidates_model->_update_candidate($this->uri->segment(3), $obj);
            redirect('candidates', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Update Candidate',
                'candidate' => $this->candidates_model->_get_candidate($this->uri->segment(3)),
                'positions' => $this->positions_model->_get_positions(),
                'partylists' => $this->partylists_model->_get_partylists()
            );
            $this->load->view('header', $obj);
            $this->load->view('candidates/update');
            $this->load->view('footer');
        }
    }

    #callback

    public function candidate_exists() {
        if ($this->candidates_model->_get_candidate_by_person(set_value('person_id'))) {
            $this->form_validation->set_message('candidate_exists', 'Candidate already exists');
            return false;
        } else {
            return true;
        }
    }

    public function position_selection_empty() {
        if (set_value('position_id') === '') {
            $this->form_validation->set_message('position_selection_empty', 'The Position field is required.');
            return false;
        } else {
            return true;
        }
    }

    public function partylist_selection_empty() {
        if (set_value('partylist_id') === '') {
            $this->form_validation->set_message('partylist_selection_empty', 'The Party List field is required.');
            return false;
        } else {
            return true;
        }
    }

    #end callback
}

/* 
 * end of file 
 * location: controllers/candidates.php 
 */
