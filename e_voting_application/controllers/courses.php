<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Courses extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Courses',
            'courses' => $this->courses_model->_get_courses()
        );
        $this->load->view('header', $obj);
        $this->load->view('courses/courses');
        $this->load->view('footer');
    }

    public function create() {
        $this->form_validation->set_rules('name', 'Course Name', 'trim|required|xss_clean|callback_course_exists');
        $this->form_validation->set_rules('description', 'Course Description', 'trim|required|xss_clean');
        $this->form_validation->set_rules('department_id', 'Department', 'trim|required|xss_clean|callback_dept_selection_empty');
        if ($this->form_validation->run()) {
            $obj = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description'),
                'department_id' => $this->input->post('department_id')
            );
            $this->courses_model->_create_course($obj);
            redirect('courses', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Add Course',
                'departments' => $this->departments_model->_get_departments()
            );
            $this->load->view('header', $obj);
            $this->load->view('courses/create');
            $this->load->view('footer');
        }
    }

    public function update() {
        $this->form_validation->set_rules('name', 'Course Name', 'trim|required|xss_clean|callback_course_exists');
        $this->form_validation->set_rules('description', 'Course Description', 'trim|required|xss_clean');
        $this->form_validation->set_rules('department_id', 'Department', 'trim|required|xss_clean|callback_dept_selection_empty');
        if ($this->form_validation->run()) {
            $obj = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description'),
                'department_id' => $this->input->post('department_id')
            );
            $this->courses_model->_update_course($this->uri->segment(3), $obj);
            redirect('courses', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Update Course',
                'course' => $this->courses_model->_get_course($this->uri->segment(3)),
                'departments' => $this->departments_model->_get_departments()
            );
            $this->load->view('header', $obj);
            $this->load->view('courses/update');
            $this->load->view('footer');
        }
    }

    #callback

    public function dept_selection_empty() {
        if (set_value('department_id') === '') {
            $this->form_validation->set_message('dept_selection_empty', 'The Department field is required.');
            return false;
        } else {
            return true;
        }
    }

    #end callback
}

/* 
 * end of file 
 * location: controllers/courses.php 
 */