<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered display" width="100%">
                    <?php if ($departments): ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Department</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($departments as $row): ?>
                                <tr>
                                    <td><?= $row->id ?></td>
                                    <td><a href="<?= base_url('department/update') . '/' . $row->id ?>"><?= $row->name ?></a></td>
                                    <td><?= $row->description ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No departments found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
                <div class="down-below">
                    <a href="<?= base_url('department/create') ?>" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading...">add department</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->load->view('feedback') ?>