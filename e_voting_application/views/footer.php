<footer>
    <div class="container">
        <p class="text-center"><?= site_copyright() ?></p>
    </div>
</footer>

<!-- Loading JS Libraries -->
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/jquery-2.1.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/holder.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/modernizr-2.7.2.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/jquery.timeago.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/tinymce/js/tinymce/tinymce.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/pace.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/datatables/js/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/datatables/js/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/custom.min.js'); ?>"></script>

</body>
</html>