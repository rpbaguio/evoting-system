<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Departments extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Departments',
            'departments' => $this->departments_model->_get_departments()
        );
        $this->load->view('header', $obj);
        $this->load->view('departments/departments');
        $this->load->view('footer');
    }

    public function create() {
        $this->form_validation->set_rules('name', 'Department Name', 'trim|required|xss_clean|callback_department_exists');
        $this->form_validation->set_rules('description', 'Department Description', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $obj = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description')
            );
            $this->departments_model->_create_department($obj);
            redirect('departments', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Add Department',
            );
            $this->load->view('header', $obj);
            $this->load->view('departments/create');
            $this->load->view('footer');
        }
    }

    public function update() {
        $this->form_validation->set_rules('name', 'Department Name', 'trim|required|xss_clean|callback_department_exists');
        $this->form_validation->set_rules('description', 'Department Description', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $obj = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description')
            );
            $this->departments_model->_update_department($this->uri->segment(3), $obj);
            redirect('departments', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Update Deparment',
                'department' => $this->departments_model->_get_department($this->uri->segment(3))
            );
            $this->load->view('header', $obj);
            $this->load->view('departments/update');
            $this->load->view('footer');
        }
    }

}

/* 
 * end of file 
 * location: controllers/departments.php 
 */