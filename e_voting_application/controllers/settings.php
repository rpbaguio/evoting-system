<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $this->form_validation->set_rules('site_title', 'Site Title', 'trim|required|xss_clean');
        $this->form_validation->set_rules('site_slogan', 'Site Slogan', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $obj = array(
                'site_title' => $this->input->post('site_title'),
                'site_slogan' => $this->input->post('site_slogan')
            );
            $this->settings_model->_update_settings($obj);
            redirect('settings', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Settings',
                'settings' => $this->settings_model->_get_settings()
            );
            $this->load->view('header', $obj);
            $this->load->view('settings');
            $this->load->view('footer');
        }
    }

}

/* 
 * end of file 
 * location: controllers/settings.php 
 */