<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <?php echo form_open('ballot'); ?>
            <div class="col-md-12 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <div class="page-header">
                    <h1><?= site_title() ?><small>&nbsp;&mdash;&nbsp;<?= $page_title ?></small></h1>
                </div>
                <?php if (position()): ?>
                    <?php foreach (position() as $row): ?> 
                        <h2><?= $row->name ?><small>&nbsp;&mdash;&nbsp;Vote for <?= convert_number_to_words(max_vote($row->id)) . nbs() . '(' . max_vote($row->id) . ')' ?></small></h2>
                        <hr class="hr-bottom" />
                        <?php echo validation_errors(); ?>
                        <?php $candidates = candidates($row->id); ?>
                        <?php $max_vote = max_vote($row->id); ?>
                        <?php $position = $row->name; ?>
                        <?php if ($candidates): ?>
                            <div class="row">
                                <?php foreach ($candidates as $row): ?>    
                                    <div class="col-md-4">
                                        <div class="ballot-form <?= $position . '_' . $max_vote ?>">
                                            <input id="<?= $position . '_' . $max_vote ?>" data-type="max" type="hidden" value="<?= $max_vote ?>">
                                            <input type="checkbox" id="candidate_<?= $row->id ?>" class="css-checkbox" name="candidate_id[]" value="<?= $row->id ?>"<?php echo set_checkbox('candidate_id', $row->id); ?> />
                                            <label for="candidate_<?= $row->id ?>" class="css-label">
                                                <img class="img-rounded pull-right" src="<?= base_url('e_voting_assets/images/pictures') . '/' . $row->picture ?>">
                                                <?= $row->f_name . nbs() . $row->l_name . br() . '<small>' . $row->party . '</small>' ?>
                                            </label>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php else: ?>
                            <p class="text text-danger">No candidates found.</p>
                        <?php endif; ?> 
                    <?php endforeach; ?>
                <?php else: ?>
                    <p class="text text-danger">No positions found.</p>
                <?php endif; ?> 
                <div class="up-top down-below">
                    <button type="button" id="btn-change-state" class="btn btn-lg btn-primary btn-animate btn-chunky btn-uppercase" data-toggle="modal" data-target="#confirmVote" data-loading-text = "loading...">vote</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<!-- Confirmation dialog -->
<div class="modal fade" id="confirmVote" role="dialog" aria-labelledby="confirmVoteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure about this?. Click <strong>CONFIRM</strong> button if YES, click <strong>CANCEL</strong> if NO.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg btn-default btn-chunky btn-animate btn-uppercase" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-lg btn-primary btn-chunky btn-animate btn-uppercase" id="confirm">Confirm</button>
            </div>
        </div>
    </div>
</div>
