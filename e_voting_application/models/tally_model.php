<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tally_Model extends CI_Model {

    public $tbl;

    public function __construct() {
        parent::__construct();

        $this->config->load('db_tbl_config');
        $this->tbl = $this->config->item('db_tbl_tally');

        if (!$this->db->table_exists($this->tbl)) {
            $this->_create_tbl_tally();
        }
    }

    public function _get_tally() {
        $this->db->select('tbl_tally.id,'
                . 'tbl_persons.f_name,'
                . 'tbl_persons.l_name,'
                . 'tbl_candidates.person_id AS candidate');
        $this->db->from($this->tbl);
        $this->db->join('tbl_persons', 'tbl_persons.id = tbl_tally.person_id');
        $this->db->join('tbl_candidates', 'tbl_candidates.person_id = tbl_tally.candidate_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function _create_tally() {
        foreach ($this->input->post('candidate_id') as $candidate_id) {
            $obj = array(
                'person_id' => person('id'),
                'candidate_id' => $candidate_id
            );
            $query = $this->db->insert($this->tbl, $obj);
        }
        return $query;
    }

    public function _count_votes_by_candidate($candidate_id) {
        $this->db->where('tbl_tally.candidate_id', $candidate_id);
        $this->db->from($this->tbl);
        $query = $this->db->count_all_results();
        return $query;
    }

    public function _create_tbl_tally() {
        $this->load->dbforge();
        $this->db->query('SET storage_engine=MYISAM;');
        $this->dbforge->add_field('id INT(11) NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('person_id VARCHAR(20) NOT NULL');
        $this->dbforge->add_field('candidate_id INT(11) NOT NULL');
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl);
    }

}

/* 
 * end of file 
 * location: models/tally_model.php 
 */
    