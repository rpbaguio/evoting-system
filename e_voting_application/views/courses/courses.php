<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered display" width="100%">
                    <?php if ($courses): ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Course</th>
                                <th>Description</th>
                                <th>Department</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($courses as $row): ?>
                                <tr>
                                    <td><?= $row->id ?></td>
                                    <td><a href="<?= base_url('course/update') . '/' . $row->id ?>"><?= $row->course ?></a></td>
                                    <td><?= $row->description ?></td>
                                    <td><?= $row->department ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No courses found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
                <div class="down-below">
                    <a href="<?= base_url('course/create') ?>" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading...">add course</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->load->view('feedback') ?>