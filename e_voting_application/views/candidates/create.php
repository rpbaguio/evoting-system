<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2>            
                    <?= $page_title ?>
                    <i class="fa fa-pencil-square-o pull-right"></i>
                </h2>
                <hr class="hr-bottom" />
                <?php echo form_open('candidate/create'); ?>
                <?php if (validation_errors()): ?>
                    <div class="alert alert-danger">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo (form_error('person_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Person ID', 'person_id'); ?>
                        <?php echo form_input(array('class' => 'form-control', 'name' => 'person_id', 'placeholder' => 'ID Number', 'value' => set_value('person_id'), 'tabindex' => '1')); ?>
                        <?= '</div>' ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?php echo (form_error('position_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Position', 'position_id'); ?>
                        <select class="form-control" name="position_id" tabindex="2">
                            <option value="">&mdash;&nbsp;Select Position&nbsp;&mdash;</option>
                            <?php foreach ($positions as $row): ?>     
                                <option value="<?= $row->id ?>" <?= set_select('position_id', $row->id) ?>><?= $row->name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?= '</div>' ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo (form_error('partylist_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Party List', 'partylist_id'); ?>
                        <select class="form-control" name="partylist_id" tabindex="3">
                            <option value="">&mdash;&nbsp;Select Party List&nbsp;&mdash;</option>
                            <?php foreach ($partylists as $row): ?>     
                                <option value="<?= $row->id ?>"<?= set_select('partylist_id', $row->id) ?>><?= $row->name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?= '</div>' ?>
                    </div>
                </div>
                <div class="up-top down-below">
                    <a href="<?= base_url('candidates'); ?>" class="btn btn-default btn-chunky btn-animate btn-uppercase">cancel</a>
                    <button type="submit" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading..." tabindex="4">register</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>