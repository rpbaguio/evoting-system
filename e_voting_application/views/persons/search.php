<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <h2><?= $page_title ?><i class="fa fa-search pull-right text-muted"></i></h2>
                <hr class="hr-bottom" />
                <?php echo form_open('persons/search'); ?>
                <?php if ($error): ?>
                    <div class="alert alert-danger"><?= $error ?></div>
                <?php endif; ?>
                <?php echo validation_errors(); ?>
                <div class="well well-sm">
                    <?php echo (form_error('id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                    <label class="control-label">Person ID</label>
                    <?php echo form_input(array('class' => 'form-control input-lg', 'name' => 'id', 'placeholder' => 'Please type Person ID number.', 'tabindex' => '1', 'value' => set_value('id'))); ?>
                    <?= '</div>' ?>
                </div>
                <button type="submit" id="btn-change-state" class="btn btn-lg btn-primary btn-block btn-animate btn-chunky btn-uppercase up-top down-below" data-loading-text="loading..." tabindex="2">search</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
