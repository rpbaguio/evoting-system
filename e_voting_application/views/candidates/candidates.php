<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered display" width="100%">
                    <?php if ($candidates): ?>
                        <thead>
                            <tr>
                                <th>ID No.</th>
                                <th>Full Name</th>
                                <th>Position</th>
                                <th>Party List</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($candidates as $row): ?>
                                <tr>
                                    <td><a href="<?= base_url('candidate/update') . '/' . $row->person_id ?>"><?= $row->person_id ?></a></td>
                                    <td><?= $row->f_name . nbs() . $row->l_name ?></td>
                                    <td><?= $row->position ?></td>
                                    <td><?= $row->party ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No candidates found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
                <div class="down-below">
                    <a href="<?= base_url('candidate/create') ?>" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading...">register candidate</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->load->view('feedback') ?>