<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <?php foreach ($partylist as $row): ?>
                    <h2><?= strtoupper($row->name) ?></h2>
                    <hr class="hr-bottom" />
                    <?php echo form_open('partylist/update/' . $this->uri->segment(3)); ?>
                    <?php if (validation_errors()): ?>
                        <div class="alert alert-danger">
                            <?php echo validation_errors(); ?>
                        </div>
                    <?php endif; ?>
                    <?php echo (form_error('name')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                    <?php echo form_label('Party List', 'name'); ?>
                    <?php echo form_input(array('class' => 'form-control', 'name' => 'name', 'placeholder' => 'Party List Name', 'value' => $row->name, 'tabindex' => '1')); ?>
                    <?= '</div>' ?>
                    <?php echo (form_error('description')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                    <?php echo form_label('Description', 'description'); ?>
                    <?php echo form_textarea(array('class' => 'form-control', 'rows' => '3', 'name' => 'description', 'placeholder' => 'Short Description', 'value' => $row->description, 'tabindex' => '2')); ?>
                    <?= '</div>' ?>
                    <div class="up-top down-below">
                        <a href="<?= base_url('partylists'); ?>" class="btn btn-default btn-chunky btn-animate btn-uppercase">cancel</a>
                        <button type="submit" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading..." tabindex="3">save changes</button>
                    </div>
                    <?php echo form_close(); ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>