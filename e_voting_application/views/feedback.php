<?php if ($this->session->flashdata('success')): ?>
    <div id="feedback" class="alert alert-success">
        <i class="fa fa-check-circle fa-lg"></i>&nbsp;<?= $this->session->flashdata('success') ?>
    </div>
<?php endif; ?>