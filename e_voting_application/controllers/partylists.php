<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Partylists extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Party Lists',
            'partylists' => $this->partylists_model->_get_partylists()
        );
        $this->load->view('header', $obj);
        $this->load->view('partylists/partylists');
        $this->load->view('footer');
    }

    public function create() {
        $this->form_validation->set_rules('name', 'Party List Name', 'trim|required|xss_clean|callback_partylist_exists');
        $this->form_validation->set_rules('description', 'Party List Description', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $obj = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description')
            );
            $this->partylists_model->_create_partylist($obj);
            redirect('partylists', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Add Party List',
            );
            $this->load->view('header', $obj);
            $this->load->view('partylists/create');
            $this->load->view('footer');
        }
    }

    public function update() {
        $this->form_validation->set_rules('name', 'Party List Name', 'trim|required|xss_clean|callback_partylist_exists');
        $this->form_validation->set_rules('description', 'Party List Description', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $obj = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description')
            );
            $this->partylists_model->_update_partylist($this->uri->segment(3), $obj);
            redirect('partylists', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Update Party List',
                'partylist' => $this->partylists_model->_get_partylist($this->uri->segment(3))
            );
            $this->load->view('header', $obj);
            $this->load->view('partylists/update');
            $this->load->view('footer');
        }
    }

}

/* 
 * end of file 
 * location: controllers/partylists.php 
 */