<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <h2><?= $page_title ?><span class="pull-right"><i class="fa fa-cogs"></i></span></h2>
                <hr class="hr-bottom" />
                <?php echo form_open(); ?>
                <?php if (validation_errors()): ?>
                    <div class="alert alert-danger">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php foreach ($settings as $row): ?>
                    <?php echo (form_error('site_title')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                    <?php echo form_label('Site Title', 'site_title'); ?>
                    <?php echo form_input(array('class' => 'form-control', 'name' => 'site_title', 'placeholder' => 'Site Title', 'value' => $row->site_title, 'tabindex' => '1')); ?>
                    <?= '</div>' ?>
                    <?php echo (form_error('site_slogan')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                    <?php echo form_label('Site Slogan', 'site_slogan'); ?>
                    <?php echo form_input(array('class' => 'form-control', 'name' => 'site_slogan', 'placeholder' => 'Site Slogan', 'value' => $row->site_slogan, 'tabindex' => '2')); ?>
                    <?= '</div>' ?>
                <?php endforeach; ?>
                <div class="up-top down-below">
                    <button type="submit" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading..." tabindex="3">save changes</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->load->view('feedback') ?>