<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <?php echo form_open('persons'); ?>
                <table class="table table-striped table-bordered persons" width="100%">
                    <?php if ($persons): ?>
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="select_all" /></th>
                                <th>ID No.</th>
                                <th>Full Name</th>
                                <th>Course</th>
                                <th>Is Verified?</th>
                                <th>Is Voted?</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($persons as $row): ?>
                                <tr>
                                    <td style="width: 1%;">
                                        <?php if ($row->is_verified == 0): ?>
                                            <input type="checkbox" id="person_<?= $row->id ?>" name="person_id[]" value="<?= $row->id ?>"<?php echo set_checkbox('person_id', $row->id); ?> />
                                        <?php else: ?>
                                            <input type="checkbox" disabled="disabled"/>
                                        <?php endif; ?>
                                    </td>
                                    <td><a href="<?= base_url('person/update') . '/' . $row->id ?>"><?= $row->id ?></a></td>
                                    <td><?= $row->f_name . nbs() . $row->l_name ?></td>
                                    <td><?= $row->course ?></td>
                                    <td><?php echo ($row->is_verified == 1) ? '<span class="label label-success">verified</span>' : '<span class="label label-danger">unverified</span>'; ?></td>
                                    <td><?php echo ($row->is_voted == 1) ? '<span class="label label-success">yes</span>' : '<span class="label label-danger">no</span>'; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No persons found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
                <div class="down-below">
                    <button type="submit" id="btn-change-state" class="btn btn-success btn-chunky btn-animate btn-uppercase" data-loading-text = "loading...">verify</button>
                    <a href="<?= base_url('person/create') ?>" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading...">register</a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<?= $this->load->view('feedback') ?>