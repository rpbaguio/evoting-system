<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        if (!logged_in()) {
            redirect('auth/signin');
        } else {
            redirect('ballot', 'refresh');
        }
    }

    public function signin() {
        if (logged_in()) {
            redirect('ballot', 'refresh');
        }

        $data['error'] = '';
        $data['page_title'] = 'Authentication';

        $this->form_validation->set_rules('person_id', 'Elector ID', 'required|callback_validated');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run()) {
            if ($this->authme->signin(set_value('person_id'))) {
                redirect('ballot', 'refresh');
            } else {
                $data['error'] = 'Your ID number is incorrect.';
            }
        }
        $this->load->view('header', $data);
        $this->load->view('signin');
        $this->load->view('footer');
    }

    public function validated() {
        if ($this->persons_model->_get_person_by_status(set_value('person_id'), 1, 0)) {
            return true;
        } else {
            $this->form_validation->set_message('validated', 'Authentication failed. Please notify the technical personnel.');
            return false;
        }
    }

    public function signout() {
        if (!logged_in()) {
            redirect('auth/signin');
        } else {
            $this->authme->signout('/');
        }
    }

}

/*
* end of file 
* location: controllers/auth.php 
*/