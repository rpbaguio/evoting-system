<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Positions extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Positions',
            'positions' => $this->positions_model->_get_positions()
        );
        $this->load->view('header', $obj);
        $this->load->view('positions/positions');
        $this->load->view('footer');
    }

    public function create() {
        $this->form_validation->set_rules('name', 'Position Name', 'trim|required|xss_clean|callback_position_exists');
        $this->form_validation->set_rules('max_vote', 'Max Vote', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $obj = array(
                'name' => $this->input->post('name'),
                'max_vote' => $this->input->post('max_vote')
            );
            $this->positions_model->_create_position($obj);
            redirect('positions', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Add Position',
            );
            $this->load->view('header', $obj);
            $this->load->view('positions/create');
            $this->load->view('footer');
        }
    }

    public function update() {
        $this->form_validation->set_rules('name', 'Position Name', 'trim|required|xss_clean|callback_position_exists');
        $this->form_validation->set_rules('max_vote', 'Max Vote', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            $obj = array(
                'name' => $this->input->post('name'),
                'max_vote' => $this->input->post('max_vote')
            );
            $this->positions_model->_update_position($this->uri->segment(3), $obj);
            redirect('positions', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Update Position',
                'position' => $this->positions_model->_get_position_by_id($this->uri->segment(3))
            );
            $this->load->view('header', $obj);
            $this->load->view('positions/update');
            $this->load->view('footer');
        }
    }

}

/* 
 * end of file 
 * location: controllers/positions.php 
 */