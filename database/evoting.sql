/*
Navicat MySQL Data Transfer

Source Server         : Local DB
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : evoting

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-07-21 01:09:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_candidates`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_candidates`;
CREATE TABLE `tbl_candidates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` varchar(20) NOT NULL,
  `position_id` int(11) NOT NULL,
  `partylist_id` int(11) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_candidates
-- ----------------------------
INSERT INTO `tbl_candidates` VALUES ('1', '12345-2014', '1', '3', '0');
INSERT INTO `tbl_candidates` VALUES ('2', '12346-2014', '1', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('3', '12347-2014', '1', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('4', '12348-2014', '2', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('5', '12349-2014', '2', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('6', '12350-2014', '2', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('7', '12351-2014', '3', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('8', '12352-2014', '3', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('9', '12353-2014', '4', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('10', '12354-2014', '4', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('11', '12355-2014', '5', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('12', '12356-2014', '6', '3', '0');

-- ----------------------------
-- Table structure for `tbl_courses`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_courses`;
CREATE TABLE `tbl_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `department_id` int(11) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_courses
-- ----------------------------
INSERT INTO `tbl_courses` VALUES ('1', 'BSIT', 'Bachelor of Science in Information Technology', '1', '0');
INSERT INTO `tbl_courses` VALUES ('2', 'BSCS', 'Bachelor of Science in Computer Science', '1', '0');
INSERT INTO `tbl_courses` VALUES ('3', 'BSN', 'Bachelor of Science in Nursing', '2', '0');

-- ----------------------------
-- Table structure for `tbl_departments`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_departments`;
CREATE TABLE `tbl_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_departments
-- ----------------------------
INSERT INTO `tbl_departments` VALUES ('1', 'CIT', 'College of Information Technology', '0');
INSERT INTO `tbl_departments` VALUES ('2', 'CEA', 'College of Engineering and Architecture', '0');

-- ----------------------------
-- Table structure for `tbl_partylists`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_partylists`;
CREATE TABLE `tbl_partylists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_partylists
-- ----------------------------
INSERT INTO `tbl_partylists` VALUES ('1', 'Independent', 'Independent Party', '0');
INSERT INTO `tbl_partylists` VALUES ('2', 'Liberal Party', 'Liberal Party', '0');
INSERT INTO `tbl_partylists` VALUES ('3', 'Lakas CMD', 'Lakas CMD', '0');

-- ----------------------------
-- Table structure for `tbl_persons`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_persons`;
CREATE TABLE `tbl_persons` (
  `id` varchar(11) NOT NULL,
  `f_name` varchar(200) NOT NULL,
  `m_name` varchar(200) NOT NULL,
  `l_name` varchar(200) NOT NULL,
  `course_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_verified` int(1) NOT NULL,
  `is_voted` int(1) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_persons
-- ----------------------------
INSERT INTO `tbl_persons` VALUES ('12345-2014', 'Raymond', 'Pendulas', 'Baguio', '1', '3', '1', '0', 'monz.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12346-2014', 'Juan', 'Dela', 'Cruz', '1', '3', '1', '1', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12347-2014', 'Jose', 'Protacio', 'Rizal', '2', '3', '1', '1', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12348-2014', 'Person 1', 'Person 1', 'Person 1', '2', '3', '1', '1', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12349-2014', 'Person 2', 'Person 2', 'Person 2', '2', '3', '1', '1', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12350-2014', 'Person 3', 'Person 3', 'Person 3', '2', '3', '1', '0', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12351-2014', 'Person 4', 'Person 4', 'Person 4', '2', '3', '1', '0', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12352-2014', 'Person 5', 'Person 5', 'Person 5', '1', '3', '1', '0', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12353-2014', 'Person 6', 'Person 6', 'Person 6', '1', '3', '1', '0', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12354-2014', 'Person 7', 'Person 7', 'Person 7', '2', '3', '1', '0', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12355-2014', 'Person 8', 'Person 8', 'Person 8', '2', '3', '1', '0', 'default.jpg', '0');
INSERT INTO `tbl_persons` VALUES ('12356-2014', 'Richie', 'Pendulas', 'Baguio', '2', '3', '1', '0', 'default.jpg', '0');

-- ----------------------------
-- Table structure for `tbl_positions`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_positions`;
CREATE TABLE `tbl_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `max_vote` int(11) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_positions
-- ----------------------------
INSERT INTO `tbl_positions` VALUES ('1', 'President', '1', '0');
INSERT INTO `tbl_positions` VALUES ('2', 'Vice-President', '1', '0');
INSERT INTO `tbl_positions` VALUES ('3', 'Secretary', '1', '0');
INSERT INTO `tbl_positions` VALUES ('4', 'Senator', '14', '0');
INSERT INTO `tbl_positions` VALUES ('5', 'Representative', '4', '0');
INSERT INTO `tbl_positions` VALUES ('6', 'Mayor', '1', '0');

-- ----------------------------
-- Table structure for `tbl_roles`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_roles`;
CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_roles
-- ----------------------------
INSERT INTO `tbl_roles` VALUES ('1', 'Administrator', '0');
INSERT INTO `tbl_roles` VALUES ('2', 'Chairman', '0');
INSERT INTO `tbl_roles` VALUES ('3', 'Voter', '0');

-- ----------------------------
-- Table structure for `tbl_role_privileges`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_role_privileges`;
CREATE TABLE `tbl_role_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_role_privileges
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_settings`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_settings`;
CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) NOT NULL,
  `site_slogan` varchar(255) NOT NULL,
  `copyright` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_settings
-- ----------------------------
INSERT INTO `tbl_settings` VALUES ('1', 'SSC Election 2014', 'Supreme Student Council', '<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-nd/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"http://localhost/e-voting/e_voting_assets/images/creative_commons/80x15.png\" /></a><br /><span xmlns:dct=\"http://purl.org/dc/terms/\" property=\"dct:title\">e-Voting System</span> by <span xmlns:cc=\"http://creativecommons.org/ns#\" property=\"cc:attributionName\">rpbaguio</span> is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-nd/4.0/\">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.');

-- ----------------------------
-- Table structure for `tbl_tally`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tally`;
CREATE TABLE `tbl_tally` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` varchar(20) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_tally
-- ----------------------------
INSERT INTO `tbl_tally` VALUES ('1', '12347-2014', '1');
INSERT INTO `tbl_tally` VALUES ('2', '12347-2014', '4');
INSERT INTO `tbl_tally` VALUES ('3', '12347-2014', '7');
INSERT INTO `tbl_tally` VALUES ('4', '12347-2014', '9');
INSERT INTO `tbl_tally` VALUES ('5', '12347-2014', '10');
INSERT INTO `tbl_tally` VALUES ('6', '12347-2014', '11');
INSERT INTO `tbl_tally` VALUES ('7', '12348-2014', '1');
INSERT INTO `tbl_tally` VALUES ('8', '12348-2014', '5');
INSERT INTO `tbl_tally` VALUES ('9', '12348-2014', '8');
INSERT INTO `tbl_tally` VALUES ('10', '12348-2014', '9');
INSERT INTO `tbl_tally` VALUES ('11', '12348-2014', '10');
INSERT INTO `tbl_tally` VALUES ('12', '12348-2014', '11');
INSERT INTO `tbl_tally` VALUES ('13', '12348-2014', '12');
INSERT INTO `tbl_tally` VALUES ('14', '12346-2014', '1');
INSERT INTO `tbl_tally` VALUES ('15', '12346-2014', '5');
INSERT INTO `tbl_tally` VALUES ('16', '12346-2014', '8');
INSERT INTO `tbl_tally` VALUES ('17', '12346-2014', '9');
INSERT INTO `tbl_tally` VALUES ('18', '12346-2014', '10');
INSERT INTO `tbl_tally` VALUES ('19', '12346-2014', '11');
INSERT INTO `tbl_tally` VALUES ('20', '12346-2014', '12');
INSERT INTO `tbl_tally` VALUES ('21', '12349-2014', '1');
INSERT INTO `tbl_tally` VALUES ('22', '12349-2014', '5');
INSERT INTO `tbl_tally` VALUES ('23', '12349-2014', '7');
INSERT INTO `tbl_tally` VALUES ('24', '12349-2014', '9');
INSERT INTO `tbl_tally` VALUES ('25', '12349-2014', '10');
INSERT INTO `tbl_tally` VALUES ('26', '12349-2014', '11');
INSERT INTO `tbl_tally` VALUES ('27', '12349-2014', '12');
