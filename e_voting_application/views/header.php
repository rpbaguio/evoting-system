<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?= $page_title ?> | e-Voting System</title>
        <link rel="shortcut icon" href="<?= base_url('e_voting_assets/images/ico/favicon.png'); ?>">
        <link rel="stylesheet" href="<?= base_url('e_voting_assets/baseui/css/style.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('e_voting_assets/bootstrap/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('e_voting_assets/bootstrap/css/bootstrap-button.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('e_voting_assets/baseui/css/blueprint.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('e_voting_assets/baseui/css/blueprint.sticky.footer.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('e_voting_assets/fontawesome/css/font-awesome.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('e_voting_assets/pace/css/themes/flash.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('e_voting_assets/datatables/css/dataTables.bootstrap.min.css'); ?>" />
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if late IE 9]>
        <script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/html5shiv.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/respond.min.js'); ?>"></script>
        <![endif]-->
    </head>
    <body>