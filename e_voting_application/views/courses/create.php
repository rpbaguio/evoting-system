<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <h2><?= $page_title ?><i class="fa fa-pencil-square-o pull-right"></i></h2>
                <hr class="hr-bottom" />
                <?php echo form_open('course/create'); ?>
                <?php if (validation_errors()): ?>
                    <div class="alert alert-danger">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-6">
                        <?php echo (form_error('name')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Course', 'name'); ?>
                        <?php echo form_input(array('class' => 'form-control', 'name' => 'name', 'placeholder' => 'Course Name', 'value' => set_value('name'), 'tabindex' => '1')); ?>
                        <?= '</div>' ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo (form_error('department_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Department', 'department_id'); ?>
                        <select class="form-control" name="department_id" tabindex="3">
                            <option value="">&mdash;&nbsp;Select Department&nbsp;&mdash;</option>
                            <?php foreach ($departments as $dept): ?>     
                                <option value="<?= $dept->id ?>" <?= set_select('department_id', $dept->id) ?>><?= $dept->name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?= '</div>' ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo (form_error('description')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Description', 'description'); ?>
                        <?php echo form_textarea(array('class' => 'form-control', 'rows' => '3', 'name' => 'description', 'placeholder' => 'Short Description', 'value' => set_value('description'), 'tabindex' => '2')); ?>
                        <?= '</div>' ?>
                    </div>
                </div>
                <div class="up-top down-below">
                    <a href="<?= base_url('courses'); ?>" class="btn btn-default btn-chunky btn-animate btn-uppercase">cancel</a>
                    <button type="submit" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading..." tabindex="3">submit</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>