<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Persons extends CI_Controller {

    public $person_id = '';

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
        $this->person_id = $this->input->post('person_id');
    }

    public function index() {
        $this->form_validation->set_rules('person_id[]', '', 'required');
        if ($this->form_validation->run()) {
            foreach ($this->person_id as $person_id) {
                $this->persons_model->_update_person($person_id, array('is_verified' => 1));
            }
            redirect('persons', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Manage Persons',
                'persons' => $this->persons_model->_get_persons()
            );
            $this->load->view('header', $obj);
            $this->load->view('persons/persons');
            $this->load->view('footer');
        }
    }

    public function search() {
        $data['error'] = '';
        $data['page_title'] = 'Search Person';

        $this->form_validation->set_rules('id', 'Person ID', 'trim|required|xss_clean|callback_voted');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run()) {
            if ($this->persons_model->_get_person_by_id(set_value('id'))) {
                redirect('person/update/' . set_value('id'), 'refresh');
            } else {
                $data['error'] = 'Person ID is incorrect.';
            }
        } else {

            $this->load->view('header', $data);
            $this->load->view('persons/search');
            $this->load->view('footer');
        }
    }

    public function create() {
        $this->form_validation->set_rules('id', 'Person ID', 'trim|required|xss_clean|callback_person_exists');
        $this->form_validation->set_rules('f_name', 'First Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('l_name', 'Last Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('course_id', 'Course', 'trim|required|xss_clean|callback_course_selection_empty');
        $this->form_validation->set_rules('role_id', 'Role', 'trim|required|xss_clean|callback_role_selection_empty');
        if ($this->form_validation->run()) {
            $obj = array(
                'id' => $this->input->post('id'),
                'f_name' => $this->input->post('f_name'),
                'm_name' => $this->input->post('m_name'),
                'l_name' => $this->input->post('l_name'),
                'course_id' => $this->input->post('course_id'),
                'role_id' => $this->input->post('role_id'),
                'picture' => 'default.jpg'
            );
            $this->persons_model->_create_person($obj);
            redirect('persons', 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Registration Form',
                'courses' => $this->courses_model->_get_courses(),
                'roles' => $this->roles_model->_get_roles()
            );
            $this->load->view('header', $obj);
            $this->load->view('persons/create');
            $this->load->view('footer');
        }
    }

    public function update() {
        $this->form_validation->set_rules('id', 'Person ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('f_name', 'First Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('l_name', 'Last Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('course_id', 'Course', 'trim|required|xss_clean|callback_course_selection_empty');
        $this->form_validation->set_rules('role_id', 'Role', 'trim|required|xss_clean|callback_role_selection_empty');
        if ($this->form_validation->run()) {
            $obj = array(
                'id' => $this->input->post('id'),
                'f_name' => $this->input->post('f_name'),
                'm_name' => $this->input->post('m_name'),
                'l_name' => $this->input->post('l_name'),
                'course_id' => $this->input->post('course_id'),
                'role_id' => $this->input->post('role_id'),
                'picture' => $this->input->post('picture')
            );
            $this->persons_model->_update_person($this->uri->segment(3), $obj);
            $this->persons_model->_update_person($this->uri->segment(3), array('is_verified' => 1));
            redirect('persons/update/' . $this->uri->segment(3), 'refresh');
        } else {
            $obj = array(
                'page_title' => 'Update Person',
                'person' => $this->persons_model->_get_person($this->uri->segment(3)),
                'courses' => $this->courses_model->_get_courses(),
                'roles' => $this->roles_model->_get_roles()
            );
            $this->load->view('header', $obj);
            $this->load->view('persons/update');
            $this->load->view('footer');
        }
    }

    public function delete() {
        
    }

    #callback

    public function voted() {
        if ($this->persons_model->_get_person_by_status(set_value('id'), 0, 0) || $this->persons_model->_get_person_by_status(set_value('id'), 1, 0)) {
            return true;
        } else {
            $this->form_validation->set_message('voted', 'Person already voted or Person ID is incorrect.');
            return false;
        }
    }

    public function person_exists() {
        if ($this->persons_model->_get_person_by_id(set_value('id'))) {
            $this->form_validation->set_message('person_exists', 'Person already exists');
            return false;
        } else {
            return true;
        }
    }

    public function course_selection_empty() {
        if (set_value('course_id') === '') {
            $this->form_validation->set_message('course_selection_empty', 'The Course field is required.');
            return false;
        } else {
            return true;
        }
    }

    public function role_selection_empty() {
        if (set_value('role_id') === '') {
            $this->form_validation->set_message('role_selection_empty', 'The Role field is required.');
            return false;
        } else {
            return true;
        }
    }

    #end callback
}

/*
     * end of file 
     * location: controllers/persons.php 
     */
    