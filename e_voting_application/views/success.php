<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <a type="button" class="close" href="<?= base_url() ?>"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                <h2 class="text-center"><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <p class="text-center">
                    <a href="<?= base_url() ?>" id="btn-change-state" class="btn btn-lg btn-primary btn-animate btn-chunky btn-uppercase" data-loading-text = "loading..." tabindex="1">close</a>
                </p>
            </div>
        </div>
    </div>
</div>
