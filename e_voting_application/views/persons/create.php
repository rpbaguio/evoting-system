<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2>            
                    <?= $page_title ?>
                    <i class="fa fa-pencil-square-o pull-right"></i>
                </h2>
                <hr class="hr-bottom" />
                <?php echo form_open('person/create'); ?>
                <?php if (validation_errors()): ?>
                    <div class="alert alert-danger">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-6">
                        <?php echo (form_error('id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Person ID', 'id'); ?>
                        <?php echo form_input(array('class' => 'form-control', 'name' => 'id', 'placeholder' => 'ID Number', 'value' => set_value('id'), 'tabindex' => '1')); ?>
                        <?= '</div>' ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php echo (form_error('f_name')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('First Name', 'f_name'); ?>
                        <?php echo form_input(array('class' => 'form-control', 'name' => 'f_name', 'placeholder' => 'First Name', 'value' => set_value('f_name'), 'tabindex' => '2')); ?>
                        <?= '</div>' ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo (form_error('m_name')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Middle Name', 'm_name'); ?>
                        <?php echo form_input(array('class' => 'form-control', 'name' => 'm_name', 'placeholder' => 'Middle Name', 'value' => set_value('m_name'), 'tabindex' => '3')); ?>
                        <?= '</div>' ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo (form_error('l_name')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Last Name', 'l_name'); ?>
                        <?php echo form_input(array('class' => 'form-control', 'name' => 'l_name', 'placeholder' => 'Last Name', 'value' => set_value('l_name'), 'tabindex' => '4')); ?>
                        <?= '</div>' ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?php echo (form_error('course_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Course', 'course_id'); ?>
                        <select class="form-control" name="course_id" tabindex="5">
                            <option value="">&mdash;&nbsp;Select Course&nbsp;&mdash;</option>
                            <?php foreach ($courses as $row): ?>     
                                <option value="<?= $row->id ?>" <?= set_select('course_id', $row->id) ?>><?= $row->course ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?= '</div>' ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo (form_error('role_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                        <?php echo form_label('Role', 'role_id'); ?>
                        <select class="form-control" name="role_id" tabindex="6">
                            <option value="">&mdash;&nbsp;Select Role&nbsp;&mdash;</option>
                            <?php foreach ($roles as $row): ?>     
                                <option value="<?= $row->id ?>"<?= set_select('role_id', $row->id) ?>><?= $row->name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?= '</div>' ?>
                    </div>
                </div>
                <div class="up-top down-below">
                    <a href="<?= base_url('persons'); ?>" class="btn btn-default btn-chunky btn-animate btn-uppercase">cancel</a>
                    <button type="submit" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading..." tabindex="8">register</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>