<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <?php foreach ($person as $person): ?>
                    <h2>
                        <?= $person->f_name . nbs() . $person->l_name ?>
                        <img class="img-circle pull-right" src="<?= base_url('e_voting_assets/images/pictures') . '/' . $person->picture ?>">
                    </h2>
                    <hr class="hr-bottom" />
                    <?php echo form_open('person/update/' . $this->uri->segment(3)); ?>
                    <?php if (validation_errors()): ?>
                        <div class="alert alert-danger">
                            <?php echo validation_errors(); ?>
                        </div>
                    <?php endif; ?>        
                    <div class="row">
                        <div class="col-md-6">
                            <?php echo (form_error('id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                            <?php echo form_label('Person ID', 'id'); ?>
                            <?php echo form_input(array('class' => 'form-control', 'name' => 'id', 'placeholder' => 'ID Number', 'value' => $person->person_id, 'tabindex' => '1')); ?>
                            <?= '</div>' ?>
                        </div>
                        <div class="col-md-3">
                            <div class = "form-group">
                            <?php echo form_label('Is Verified?', 'is_verified'); ?>
                            <?php echo ($person->is_verified == 1) ? '<p><span class="label label-success">verified</span></p>' : '<p><span class="label label-danger">unverified</span></p>'; ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class = "form-group">
                                <?php echo form_label('Is Voted?', 'is_voted'); ?>
                                <?php echo ($person->is_voted == 1) ? '<p><span class="label label-success">yes</span></p>' : '<p><span class="label label-danger">no</span></p>'; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo (form_error('f_name')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                            <?php echo form_label('First Name', 'f_name'); ?>
                            <?php echo form_input(array('class' => 'form-control', 'name' => 'f_name', 'placeholder' => 'First Name', 'value' => $person->f_name, 'tabindex' => '2')); ?>
                            <?= '</div>' ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo (form_error('m_name')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                            <?php echo form_label('Middle Name', 'm_name'); ?>
                            <?php echo form_input(array('class' => 'form-control', 'name' => 'm_name', 'placeholder' => 'Middle Name', 'value' => $person->m_name, 'tabindex' => '3')); ?>
                            <?= '</div>' ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo (form_error('l_name')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                            <?php echo form_label('Last Name', 'l_name'); ?>
                            <?php echo form_input(array('class' => 'form-control', 'name' => 'l_name', 'placeholder' => 'Last Name', 'value' => $person->l_name, 'tabindex' => '4')); ?>
                            <?= '</div>' ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php echo (form_error('course_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                            <?php echo form_label('Course', 'course_id'); ?>
                            <select class="form-control" name="course_id" tabindex="5">
                                <option value="<?= $person->course_id ?>"<?= set_select('course_id', $person->course_id) ?>><?= $person->course ?></option>
                                <?php foreach ($courses as $row): ?>     
                                    <option value="<?= $row->id ?>" <?= set_select('course_id', $row->id) ?>><?= $row->course ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= '</div>' ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo (form_error('role_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                            <?php echo form_label('Role', 'role_id'); ?>
                            <select class="form-control" name="role_id" tabindex="6">
                                <option value="<?= $person->role_id ?>"<?= set_select('role_id', $person->role_id) ?>><?= $person->role ?></option>
                                <?php foreach ($roles as $row): ?>     
                                    <option value="<?= $row->id ?>"<?= set_select('role_id', $row->id) ?>><?= $row->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= '</div>' ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php echo (form_error('picture')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                            <?php echo form_label('Picture', 'picture'); ?>
                            <?php echo form_input(array('class' => 'form-control', 'name' => 'picture', 'placeholder' => 'Picture', 'value' => $person->picture, 'tabindex' => '7')); ?>
                            <?= '</div>' ?>
                        </div>
                    </div>
                    <div class="up-top down-below">
                        <a href="<?= base_url('person/search'); ?>" class="btn btn-default btn-chunky btn-animate btn-uppercase"><i class="fa fa-search"></i>&nbsp;search</a>
                        <?php echo ($person->is_verified == 1) ? '<button type="submit" id="btn-change-state" class="btn btn-success btn-chunky btn-animate btn-uppercase" data-loading-text = "loading..." tabindex="8" disabled>verified</button>' : '<button type="submit" id="btn-change-state" class="btn btn-success btn-chunky btn-animate btn-uppercase" data-loading-text = "loading..." tabindex="8">verify</button>'; ?>
                        <button type="submit" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase pull-right" data-loading-text = "loading..." tabindex="9">save changes</button>
                    </div>
                    <?php echo form_close(); ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>