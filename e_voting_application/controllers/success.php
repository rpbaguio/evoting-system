<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Success extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
    }

    public function index() {
        $obj = array(
            'page_title' => 'Thank You for Voting!'
        );
        $this->load->view('header', $obj);
        $this->load->view('success');
        $this->load->view('footer');
    }

}

/* 
 * end of file 
 * location: controllers/success.php 
 */