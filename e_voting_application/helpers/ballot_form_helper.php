<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * Usage: 
 * 1] Load helper in controller or autoload 
 * $this->load->helper('ballot_form'); 
 * 2] Call the function in view: 
 * <?=candidates(); ?>
 */

if (!function_exists('position') || !function_exists('candidates') || !function_exists('voter') || !function_exists('count_votes') || !function_exists('site_title') || !function_exists('site_slogan') || !function_exists('max_vote') || !function_exists('convert_number_to_words')) {

    function position() {
        $CI = & get_instance();

        $CI->load->model('positions_model');

        $obj = $CI->positions_model->_get_positions();

        return $obj;
    }

    function candidates($id) {
        $CI = & get_instance();

        $CI->load->model('candidates_model');

        $obj = $CI->candidates_model->_get_candidates_by_position($id);

        return $obj;
    }

    function voter($id) {
        $CI = & get_instance();

        $CI->load->model('persons_model');

        $obj = $CI->persons_model->_get_person_by_id($id);

        return $obj;
    }

    function max_vote($id) {
        $CI = & get_instance();

        $CI->load->model('positions_model');

        $obj = $CI->positions_model->_get_position_by_id($id);

        if ($obj) {
            foreach ($obj as $row) {
                return $row->max_vote;
            }
        }
    }

    function count_votes($id) {
        $CI = & get_instance();

        $CI->load->model('tally_model');

        $obj = $CI->tally_model->_count_votes_by_candidate($id);

        return $obj;
    }

    function site_title() {
        $CI = & get_instance();

        $CI->load->model('settings_model');

        $obj = $CI->settings_model->_get_settings();

        foreach ($obj as $row) {
            return $row->site_title;
        }
    }

    function site_slogan() {
        $CI = & get_instance();

        $CI->load->model('settings_model');

        $obj = $CI->settings_model->_get_settings();

        foreach ($obj as $row) {
            return $row->site_slogan;
        }
    }

    function site_copyright() {
        $CI = & get_instance();

        $CI->load->model('settings_model');

        $obj = $CI->settings_model->_get_settings();

        foreach ($obj as $row) {
            return $row->copyright;
        }
    }

    function convert_number_to_words($number) {

        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

}

/* End of file: ballot_form_helper.php */
/* Location: application/helpers/ballot_form_helper.php */