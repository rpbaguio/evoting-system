<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Settings_Model extends CI_Model {

    public $tbl;

    public function __construct() {
        parent::__construct();

        $this->config->load('db_tbl_config');
        $this->tbl = $this->config->item('db_tbl_settings');

        if (!$this->db->table_exists($this->tbl)) {
            $this->_create_tbl_settings();
            $this->_default_settings();
        }
    }

    public function _get_settings() {
        $this->db->select('*');
        $this->db->from($this->tbl);
        $query = $this->db->get();
        return $query->result();
    }

    public function _get_setting($id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function _update_settings($obj) {
        $this->db->update($this->tbl, $obj);
        if ($this->db->affected_rows() > 0) {
            return $this->session->set_flashdata('success', 'Successfully updated!');
        }
    }

    public function _default_settings() {
        $title = 'Site Title';
        $slogan = 'Site Slogan';
        $copyright = '<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="' . base_url('e_voting_assets/images/creative_commons') . '/' . '80x15.png' . '" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">e-Voting System</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">rpbaguio</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.';
        $obj = array(
            'site_title' => $title,
            'site_slogan' => $slogan,
            'copyright' => $copyright
        );
        $query = $this->db->insert($this->tbl, $obj);
        return $query;
    }

    public function _create_tbl_settings() {
        $this->load->dbforge();
        $this->db->query('SET storage_engine=MYISAM;');
        $this->dbforge->add_field('id INT(11) NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('site_title VARCHAR(255) NOT NULL');
        $this->dbforge->add_field('site_slogan VARCHAR(255) NOT NULL');
        $this->dbforge->add_field('copyright VARCHAR(1000) NOT NULL');
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl);
    }

}

/* 
 * end of file 
 * location: models/settings_model.php 
 */
    