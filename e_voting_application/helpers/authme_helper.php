<?php

function logged_in() {
    $CI = & get_instance();
    $CI->load->library('authme');

    return $CI->authme->logged_in();
}

function person($key = '') {
    $CI = & get_instance();
    $CI->load->library('session');

    $person = $CI->session->userdata('person');
    if ($key && isset($person->$key)) {
        return $person->$key;
    }
    return $person;
}

/* End of file: authme_helper.php */
/* Location: application/helpers/authme_helper.php */