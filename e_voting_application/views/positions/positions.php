<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered display" width="100%">
                    <?php if ($positions): ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Position</th>
                                <th>Vote Allowed</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($positions as $row): ?>
                                <tr>
                                    <td><?= $row->id ?></td>
                                    <td><a href="<?= base_url('position/update') . '/' . $row->id ?>"><?= $row->name ?></a></td>
                                    <td><?= $row->max_vote ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No positions found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
                <div class="down-below">
                    <a href="<?= base_url('position/create') ?>" id="btn-change-state" class="btn btn-primary btn-chunky btn-animate btn-uppercase" data-loading-text = "loading...">add position</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->load->view('feedback') ?>