$(function() {
    //Back to Top
    function pageup() {
        $(window).scroll(function() {
            var up = $(this);
            if (up.scrollTop() !== 0) {
                $('#pageup').fadeIn();
            } else {
                $('#pageup').fadeOut();
            }
        });
        $('#pageup').click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
        });
    }

    //Nav Tab
    function navtabs() {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            e.target; // activated tab
            e.relatedTarget; // previous tab
        });
    }

    function button_change_state() {
        /* Button Change State */
        $('#btn-change-state').click(function() {
            var btn = $(this);
            btn.button('loading');
            setTimeout(function() {
                btn.button('reset');
            }, 3000);
        });
    }

    //Holder.js
    function holder() {
        Holder.add_theme("bright", {background: "#EEE", foreground: "#777", size: 30});
    }

    //Timeago
    function timeago() {
        $("time").timeago();
    }

    function tooltip() {
        // Tooltip
        $('body').tooltip({
            selector: '[data-tooltip=tooltip]',
            container: 'body'
        });
    }

    function popover() {
        // Popover
        $("#popover").popover({
            trigger: "hover",
            html: true,
            content: function() {
                return;
            }
        });
    }

    function wysiwyg() {
        // Tinymce WYSIWYG
        tinymce.init({
            selector: "textarea#editor",
            skin: "lightgray",
            theme: "modern",
            menubar: false, // Disable all menus
            statusbar: true, // Disable status bar
            toolbar_items_size: 'medium',
            forced_root_block: false, // Remove <p> tag
            force_p_newlines: false,
            remove_linebreaks: false,
            force_br_newlines: false,
            remove_trailing_nbsp: false,
            verify_html: false,
            relative_urls: true,
            plugins: [
                "wordcount",
                "link",
                "advlist",
                "autolink",
                "preview",
                "code"
            ],
            toolbar: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink | preview code"
        });
        // Prevent bootstrap dialog from blocking focusin
        $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });
    }

    //Feedback Message
    function feedback() {
        $('#feedback').animate({
            right: '75px'
        }, 500).animate({
            right: '0px'
        }, 500).fadeOut(3000);
    }

    //Datatables      
    function datatables() {
        $('table.persons').dataTable({
            "order": [[2, "asc"]],
            "columnDefs": [
                {"orderable": false, "targets": [0, 1]}
            ]
        });
        $('table.display').dataTable({
            "order": [[0, "asc"]]
        });
    }

    //Maximum number of checkbox selected
    function checkbox_limit() {
        var data = [];
        $('input[data-type="max"]').each(function() {
            data.push({
                id: $(this).attr("id"),
                value: $(this).attr("value")
            });
        });
        $.each(data, function() {
            var pos = this.id;
            var max = this.value;
            var checkboxes = $('.' + pos + ' ' + 'input[type="checkbox"]');
            checkboxes.change(function() {
                var current = checkboxes.filter(':checked').length;
                checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
            });
        });
    }

    //Confirmation dialog
    function dialog() {
        $('#confirmVote').on('show.bs.modal', function(e) {
            //$message = $(e.relatedTarget).attr('data-message');
            //$(this).find('.modal-body p').text($message);
            //$title = $(e.relatedTarget).attr('data-title');
            //$(this).find('.modal-title').text($title);

            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
        });

        //Form confirm (yes/ok) handler, submits form
        $('#confirmVote').find('.modal-footer #confirm').on('click', function() {
            $(this).data('form').submit();
        });
    }

    //Select all checkboxes
    function selectall_checkbox() {
        $('#select_all').click(function() {
            var c = this.checked;
            $(':checkbox').prop('checked', c);
        });
        $('tr').click(function(event) {
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        });
    }

    //Execute only if DOM is ready
    pageup();
    navtabs();
    button_change_state();
    holder();
    timeago();
    tooltip();
    popover();
    wysiwyg();
    feedback();
    datatables();
    checkbox_limit();
    dialog();
    selectall_checkbox();
});