<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top down-below" style="padding: 10px 20px;">
                <div class="page-header">
                    <h1><?= site_title() ?><small>&nbsp;&mdash;&nbsp;<?= $page_title ?></small></h1>
                </div>
                <div class="down-below">
                    <?php if (position()): ?>
                        <?php foreach (position() as $position): ?> 
                            <h2><?= $position->name ?></h2>
                            <hr class="hr-bottom" />
                            <?php if (candidates($position->id)): ?>
                                <div class="row">
                                    <?php foreach (candidates($position->id) as $candidate): ?>    
                                        <div class="col-md-4">
                                            <div class="result-form">
                                                <ul class="list-inline">
                                                    <li><img class="img-rounded" src="<?= base_url('e_voting_assets/images/pictures') . '/' . $candidate->picture ?>"></li>
                                                    <li><?= ($candidate->f_name . nbs() . $candidate->l_name) ?></li>
                                                    <li><span class="label label-info"><?= (count_votes($candidate->id) > 1) ? number_format(count_votes($candidate->id)) . '<small>&nbsp;(votes)</small>' : number_format(count_votes($candidate->id)) . '<small>&nbsp;(vote)</small>' ?></span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php else: ?>
                                <p class="text text-danger">No candidates found.</p>
                            <?php endif; ?> 
                        <?php endforeach; ?>
                    <?php else: ?>
                        <p class="text text-danger">No positions found.</p>
                    <?php endif; ?> 
                </div>
            </div>
        </div>
    </div>
</div>
